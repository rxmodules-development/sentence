$(document).ready(function(){
	
	// global variables
	var typingTimer, tooltipTimer;
	var typingDelay = 1500;
	var tooltipDelay = 3000;
	
	init();
	
	// initialisation of page
	function init() {
		$('.sentence-container > div:first-child').addClass('shown'); //initialise the first section
		get_section = $('div.shown');
		get_section.find('input.answer').focus(); //set focus to the first answer (if is input)
		
		startTooltipTimer(get_section);
	}
	
	// start the timer for the tooltip
	function startTooltipTimer(get_section){
		tooltipTimer = setTimeout(function(){
			showTooltip(get_section)
		}, tooltipDelay); 	
	}
	
	// clear the tooltip timer
	function clearTooltipTimer(){
		clearTimeout(tooltipTimer);
	}
	
	//show the tooltip
	function showTooltip(section) {
		section.find('.tooltip').addClass('active');
	}
	
	//hide the tooltip
	function hideTooltip(section) {
		section.find('.tooltip').removeClass('active');
	}
	
	// start the timer for the typing
	function startTypingTimer(get_input){
		typingTimer = setTimeout(function(){
			doneTyping(get_input)
		}, typingDelay);
	}
	
	// clear the typing timer
	function clearTypingTimer(){
		clearTimeout(typingTimer);
	}
	
	// validate the input boxes
	function validateInput(input){
		get_section = input.parents('.section');
		
		if (input.hasClass('email')) {
			if (validateEmail(input.val())) {
				get_section.removeClass('invalid').addClass('valid');
				return true;
			} else {
				get_section.removeClass('valid').addClass('invalid');
				return false;
			}
		} else {
			if (input.val()) {
				get_section.removeClass('invalid').addClass('valid');
				return true;
			} else {
				get_section.removeClass('valid').addClass('invalid');
				return false;
			}
		} 
	}
	
	// validate email and return
	function validateEmail(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}
	
	// use sizer span to size input boxes. Passing "max" as true gets rid of the 20px right side buffer used to stop overflow issues
	function setInputSize(input, max) {
		
		max = max || false;
		
		$('#sizer').text(input.val());
		
		if (max) {
			if ($('#sizer').width() > 50) {
				input.width($('#sizer').width());
			}	
		} else {
			if ($('#sizer').width() > 50) {
				input.width($('#sizer').width() + 20);
			} else {
				input.width(70);
			}	
		}
	}

	
	// Change the state on inputs on keypresses
	$('input').keyup(function(e){
		get_input = $(this);
		
		// clear any invalid / valid states
		get_section = get_input.parents('.section');
		get_section.removeClass('invalid valid');
		
		// reset tooltips and timers
	    hideTooltip(get_section);
	    clearTooltipTimer();
	    clearTypingTimer();
	    
	    var valid_input = validateInput(get_input);
	    
	    if (e.which == 13) {
		    if (valid_input) {
			    get_input.blur();
			 	doneTyping(get_input);
		    }
	    } else {
		    setInputSize(get_input);
		 	if (valid_input) {
			    startTypingTimer(get_input);
		    }   
	    }
	});
	
	$('input').focus(function(){
		get_input = $(this);
		
		// clear any invalid / valid states
		get_section = get_input.parents('.section');
		get_section.removeClass('invalid valid complete');
	})
	
	$('input').blur(function(){
		get_input = $(this);
		get_section = get_input.parents('.section');
		
		var valid_input = validateInput(get_input);
		
		if (valid_input) {
			get_section.addClass('valid complete');
		} else {
			get_section.addClass('invalid');
		}
		// clear any invalid / valid states
		
	})
	
	// change the state when changing an input
	$('select').change(function(){
		get_input = $(this);
		
		// clear any invalid / valid states
		get_section = get_input.parents('.section');
		get_section.removeClass('invalid valid');
		
		hideTooltip(get_section);
		clearTimeout(tooltipTimer);
		
		if (get_input.val() == 'custom') {
			get_input.siblings('.custom_answer').addClass('active').focus();
		} else if ($(this).val() != 'initial') {
			get_input.siblings('.custom_answer').removeClass('active');
			get_section.addClass('valid');
			
			doneTyping(get_input);
		}
		
		setInputSize(get_input, true);
	})


	// show the dropdown again when reselecting a custom answer field
	$('input.custom_answer').on('focus', function(){
		$(this).siblings('.answer').show();
	})
	
	
	function doneTyping(get_input) {
		get_section = get_input.parents('.section');
		
		setInputSize(get_input, true);
		
		if (get_input.hasClass('custom_answer')) {
			get_input.siblings('select').hide();
		}
		
		get_section.addClass('complete');
	    
	    next_section = get_section.next();
	    next_section.addClass('shown')
	    next_input = next_section.find('input.answer')

	    setTimeout(function(){
		    next_input.focus()
		}, 200);
		
		startTooltipTimer(next_section)
	}
	
	
	
		
	$('.submission button').click(function(){
		if ($('.invalid').length > 0) {
			$('.invalid').find('.answer').focus();
		} else {
			$('.sentence-container').addClass('sending');
			setTimeout(function(){
				$('.sent-container').addClass('sent');	
			}, 300)	
		}
	})
	
	$('.reset').click(function(){
		$('select')[0].selectedIndex = 0;
		$('.answer').val('');
		$('.answer').removeClass('valid invalid')
		$('.sentence-container div').removeClass('valid shown complete');
		$('.sentence-container div:first-child').addClass('shown');
		setTimeout(function(){$('.sentence-container > div:first-child').children('.answer').focus()}, 200);
		
		$('.sent-container').removeClass('sent');
		setTimeout(function(){
			$('.sentence-container').removeClass('sending');	
		}, 300)
	})
	
	
})